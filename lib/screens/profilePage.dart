import 'package:first/common/images.dart';
import 'package:first/common/my_info.dart';
import 'package:first/screens/view.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class profilePage extends StatefulWidget {
  @override
  _profilePageState createState() => _profilePageState();
}

class _profilePageState extends State<profilePage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: SingleChildScrollView(
        child: Container(
          child: Column(
            children: <Widget>[
              SafeArea(
                child: Padding(
                  padding: EdgeInsets.symmetric(horizontal: 20, vertical: 10),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Container(child: Image.asset('assets/images/04.jpg')),
                      Container(
                        child: Row(
                          children: <Widget>[
                            Icon(
                              Icons.add_location,
                              color: Color(0xff4482FD),
                              size: 20,
                            ),
                            Text(
                              'Los Angle, US',
                              style: TextStyle(
                                fontSize: 16,
                                fontWeight: FontWeight.w600,
                              ),
                            )
                          ],
                        ),
                      ),
                      Container(child: Image.asset('assets/images/05.jpg')),
                    ],
                  ),
                ),
              ),
              SizedBox(
                height: 20,
              ),
              Padding(
                padding: EdgeInsets.symmetric(horizontal: 20),
                child: Container(
                  child: Column(
                    children: <Widget>[
                      TextField(
                        decoration: InputDecoration(
                          contentPadding: EdgeInsets.symmetric(vertical: 20),
                          filled: true,
                          fillColor: Color(0xffF4F6FD),
                          prefixIcon: Icon(
                            Icons.search,
                            color: Color(0xff4485FD),
                          ),
                          hintText: "Describe Your Issue...",
                          enabledBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(50),
                            borderSide: BorderSide(
                              color: Color(0xffF4F6FD),
                            ),
                          ),
                          focusedBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(50),
                            borderSide: BorderSide(
                              color: Color(0xffF4F6FD),
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ),
              SizedBox(
                height: 25,
              ),
              Padding(
                padding: EdgeInsets.symmetric(horizontal: 20),
                child: Container(
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Column(
                        children: <Widget>[
                          Container(
                            height: 60,
                            width: MediaQuery.of(context).size.width / 4 - 40,
                            decoration: BoxDecoration(
                              color: Color(0xff4485FD),
                              borderRadius: BorderRadius.circular(10),
                            ),
                            child: Icon(
                              Icons.account_balance,
                              color: Colors.white,
                              size: 30,
                            ),
                          ),
                          SizedBox(
                            height: 10,
                          ),
                          Text(
                            'Hospitals',
                            style: TextStyle(
                              fontSize: 16,
                              fontWeight: FontWeight.w600,
                            ),
                          ),
                        ],
                      ),
                      Column(
                        children: <Widget>[
                          Container(
                            height: 60,
                            width: MediaQuery.of(context).size.width / 4 - 40,
                            decoration: BoxDecoration(
                              color: Color(0xffA584FF),
                              borderRadius: BorderRadius.circular(10),
                            ),
                            child: Icon(
                              Icons.airline_seat_flat,
                              color: Colors.white,
                              size: 30,
                            ),
                          ),
                          SizedBox(
                            height: 10,
                          ),
                          Text(
                            'Pharmachy',
                            style: TextStyle(
                              fontSize: 16,
                              fontWeight: FontWeight.w600,
                            ),
                          ),
                        ],
                      ),
                      Column(
                        children: <Widget>[
                          Container(
                            height: 60,
                            width: MediaQuery.of(context).size.width / 4 - 40,
                            decoration: BoxDecoration(
                              color: Color(0xffFF7854),
                              borderRadius: BorderRadius.circular(10),
                            ),
                            child: Icon(
                              Icons.beach_access,
                              color: Colors.white,
                              size: 30,
                            ),
                          ),
                          SizedBox(
                            height: 10,
                          ),
                          Text(
                            'Consult',
                            style: TextStyle(
                              fontSize: 16,
                              fontWeight: FontWeight.w600,
                            ),
                          ),
                        ],
                      ),
                      Column(
                        children: <Widget>[
                          Container(
                            height: 60,
                            width: MediaQuery.of(context).size.width / 4 - 40,
                            decoration: BoxDecoration(
                              color: Color(0xffFFB74C),
                              borderRadius: BorderRadius.circular(10),
                            ),
                            child: Icon(
                              Icons.restaurant,
                              color: Colors.white,
                              size: 30,
                            ),
                          ),
                          SizedBox(
                            height: 10,
                          ),
                          Text(
                            'More',
                            style: TextStyle(
                              fontSize: 16,
                              fontWeight: FontWeight.w600,
                            ),
                          ),
                        ],
                      ),
                    ],
                  ),
                ),
              ),
              SizedBox(
                height: 20,
              ),
              Column(
                children: <Widget>[
                  Padding(
                    padding: EdgeInsets.symmetric(horizontal: 20),
                    child: Container(
                      height: 180,
                      width: MediaQuery.of(context).size.width * 1,
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(0),
                        color: Color(0xffFF7854),
                      ),
                      child: Stack(
                        alignment: Alignment.centerLeft,
                        children: <Widget>[
                          Positioned(
                            right: 0,
                            top: 0,
                            child: Image.asset(
                              'assets/images/first.png',
                              fit: BoxFit.cover,
                              height: 180,
                            ),
                          ),
                          Container(
                            width: MediaQuery.of(context).size.width * 0.5,
                            child: Padding(
                              padding: EdgeInsets.fromLTRB(15, 15, 0, 0),
                              child: Column(
                                mainAxisAlignment: MainAxisAlignment.start,
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: <Widget>[
                                  Text(
                                    'All you need to know about Coronavirus!',
                                    style: TextStyle(
                                      fontSize: 25,
                                      fontWeight: FontWeight.w800,
                                      color: Colors.white,
                                    ),
                                  ),
                                  SizedBox(height: 10,),
                                  Container(
                                    child: RaisedButton(
                                      onPressed: (){},
                                      color: Colors.white,
                                      shape: RoundedRectangleBorder(
                                        borderRadius: BorderRadius.circular(10),
                                      ),
                                      child: Text(
                                        'Know More'
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                ],
              ),
              SizedBox(height: 20,),
              Padding(
                padding: EdgeInsets.symmetric(horizontal: 20),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Container(
                      child: Text(
                        'Top Doctors',
                        style: TextStyle(
                          fontSize: 25,
                          fontWeight: FontWeight.w800,
                        ),
                      ),
                    ),
                    Container(
                      width: 92,
                      child: Row(
                        children: <Widget>[
                          Text('View All', style: TextStyle(
                            fontSize: 16,
                            fontWeight: FontWeight.w600,
                          ),),
                          SizedBox(width: 8,),
                          Container(
                            height: 25,
                            width: 25,
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(5),
                              color: Color(0xffFF7854),
                            ),
                            child: Icon(Icons.arrow_forward_ios, color: Colors.white,size: 15,),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
              SizedBox(height: 10,),
              Padding(
                padding: EdgeInsets.symmetric(horizontal: 20),
                child: Stack(
                  children: <Widget>[
                    Container(
                      padding: EdgeInsets.symmetric(horizontal: 10, vertical: 10),
                      height: 105,
                      decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.circular(15),
                          boxShadow: [new BoxShadow(
                            color: Color(0xffF5F5F5),
                            blurRadius: 20.0,
                          ),]
                      ),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Image.asset('assets/images/06.jpg'),
                          Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              Padding(
                                padding: EdgeInsets.fromLTRB(15, 5, 0, 0),
                                child: Text(
                                  'Dr Anaiya Sharma',
                                  style: TextStyle(
                                    fontSize: 20,
                                    fontWeight: FontWeight.w600,
                                  ),
                                ),
                              ),
                              Padding(
                                padding: EdgeInsets.fromLTRB(15, 5, 0, 0),
                                child: Text(
                                  'Heart Surgent - Neon Hospital',
                                  style: TextStyle(
                                  ),
                                ),
                              ),
                              Padding(
                                padding: EdgeInsets.fromLTRB(15, 10, 0, 0),
                                child: Row(
                                  children: <Widget>[
                                    Icon(Icons.star, size: 17,),
                                    SizedBox(width: 5,),
                                    Text('4.3'),
                                    SizedBox(width: 20,),
                                    Icon(Icons.watch_later, color: Color(0xffFF7854),size: 17,),
                                    SizedBox(width: 5,),
                                    Text('09:00 - 11:00 AM'),
                                  ],
                                ),
                              ),
                            ],
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
              SizedBox(height: 10,),
              Padding(
                padding: EdgeInsets.symmetric(horizontal: 20),
                child: Stack(
                  children: <Widget>[
                    Container(
                      padding: EdgeInsets.symmetric(horizontal: 10, vertical: 10),
                      height: 105,
                      decoration: BoxDecoration(
                          color: Colors.white,
                          borderRadius: BorderRadius.circular(15),
                          boxShadow: [new BoxShadow(
                            color: Color(0xffF5F5F5),
                            blurRadius: 20.0,
                          ),]
                      ),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Image.asset('assets/images/07.jpg'),
                          Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              Padding(
                                padding: EdgeInsets.fromLTRB(15, 5, 0, 0),
                                child: Text(
                                  'Dr Jhon Simon',
                                  style: TextStyle(
                                    fontSize: 20,
                                    fontWeight: FontWeight.w600,
                                  ),
                                ),
                              ),
                              Padding(
                                padding: EdgeInsets.fromLTRB(15, 5, 0, 0),
                                child: Text(
                                  'Heart Surgent - Neon Hospital',
                                  style: TextStyle(
                                  ),
                                ),
                              ),
                              Padding(
                                padding: EdgeInsets.fromLTRB(15, 10, 0, 0),
                                child: Row(
                                  children: <Widget>[
                                    Icon(Icons.star, size: 17,),
                                    SizedBox(width: 5,),
                                    Text('4.3'),
                                    SizedBox(width: 20,),
                                    Icon(Icons.watch_later, color: Color(0xffFF7854),size: 17,),
                                    SizedBox(width: 5,),
                                    Text('09:00 - 11:00 AM'),
                                  ],
                                ),
                              ),
                            ],
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
              SizedBox(height: 10,),
              Padding(
                padding: EdgeInsets.symmetric(horizontal: 20),
                child: Stack(
                  children: <Widget>[
                    Container(
                      padding: EdgeInsets.symmetric(horizontal: 10, vertical: 10),
                      height: 105,
                      decoration: BoxDecoration(
                          color: Colors.white,
                          borderRadius: BorderRadius.circular(15),
                          boxShadow: [new BoxShadow(
                            color: Color(0xffF5F5F5),
                            blurRadius: 20.0,
                          ),]
                      ),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Image.asset('assets/images/08.jpg'),
                          Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              Padding(
                                padding: EdgeInsets.fromLTRB(15, 5, 0, 0),
                                child: Text(
                                  'Dr Alex Smith',
                                  style: TextStyle(
                                    fontSize: 20,
                                    fontWeight: FontWeight.w600,
                                  ),
                                ),
                              ),
                              Padding(
                                padding: EdgeInsets.fromLTRB(15, 5, 0, 0),
                                child: Text(
                                  'Heart Surgent - Neon Hospital',
                                  style: TextStyle(
                                  ),
                                ),
                              ),
                              Padding(
                                padding: EdgeInsets.fromLTRB(15, 10, 0, 0),
                                child: Row(
                                  children: <Widget>[
                                    Icon(Icons.star, size: 17,),
                                    SizedBox(width: 5,),
                                    Text('5'),
                                    SizedBox(width: 20,),
                                    Icon(Icons.watch_later, color: Color(0xffFF7854),size: 17,),
                                    SizedBox(width: 5,),
                                    Text('09:00 - 11:00 AM'),
                                  ],
                                ),
                              ),
                            ],
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
              SizedBox(height: 40,),
            ],
          ),
        ),
      ),
    );
  }
}
