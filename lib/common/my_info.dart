import 'package:first/common/rounded_image.dart';
import 'package:flutter/material.dart';

class MyInfo extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Expanded(
      child: Column(
        children: <Widget>[
          RoundedImage(
            imagePath: 'assets/images/anne.jpeg',
            size: Size.fromWidth(120.0),
          )
        ],
      ),
    );
  }
}
