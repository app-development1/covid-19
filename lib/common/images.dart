import 'package:flutter/material.dart';

class AssetsImages extends StatelessWidget {
  final imageUrl;

  const AssetsImages({Key key, @required this.imageUrl}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: <Widget>[
        Image.asset(
          imageUrl,
          width: double.maxFinite,
          height: double.maxFinite,
          fit: BoxFit.fill,
        ),
        Container(
          color: Theme.of(context).primaryColor.withOpacity(0.85),
        )
      ],
    );
  }
}
